import copy
import datetime
import json
import inspect
import os
import random

def datetime_to_fiware(time_as_datetime:datetime) -> str:
    try:
        dt = time_as_datetime
        return str(dt.year) + '-' + str(dt.month).zfill(2) + '-' + str(dt.day).zfill(2) + 'T' + str(dt.hour).zfill(
            2) + ':' + str(dt.minute).zfill(2) + ':' + str(dt.second).zfill(2) + 'Z'
        return time_to_fiware(time_as_datetime.timestamp())
    except Exception as e:
        print('datetime_to_fiware()-' + str(e))

    return ''

def fiware_to_datetime(fiware_time)->datetime:
    try:
        if '.' in fiware_time:
            return datetime.datetime.strptime(fiware_time, '%Y-%m-%dT%H:%M:%S.%fZ')

        return datetime.datetime.strptime(fiware_time, '%Y-%m-%dT%H:%M:%SZ')
    except Exception as e:
        # no seconds?
        return datetime.datetime.strptime(fiware_time, '%Y-%m-%dT%H:%MZ')

    return ''


pilot_model = {}

def delete_pilot(name:str) -> bool:
    if name in pilot_model:
        pilot_model.pop(name)
        return True

    return False

def clear():
    pilot_model = {}

def add_pilot(name:str) ->bool:
    if name not in pilot_model:
        pilot_model[name] = {}

        return True

    return False

def add_sensor_to_pilot(pilot:str, sensor:str) -> bool:
    if pilot in pilot_model and sensor not in pilot_model[pilot]:
        pilot_model[pilot][sensor] = {}

        pilot_model[pilot][sensor]['timestamp'] = ''
        pilot_model[pilot][sensor]['timestep'] = 15 #minutes
        pilot_model[pilot][sensor]['min_val'] = 1
        pilot_model[pilot][sensor]['max_val'] = 2
        pilot_model[pilot][sensor]['index'] = 0

        return True

    return False

def delete_sensor_from_pilot(pilot:str, sensor:str) -> bool:
    if pilot in pilot_model and sensor in pilot_model[pilot]:
        pilot_model[pilot].pop(sensor)
        return True

    return False

def reset_pilot(pilot:str, timestamp:str):

    if timestamp != '' and isinstance(timestamp, str):
        if pilot in pilot_model:
            for sensor in pilot_model[pilot]:
                pilot_model[pilot][sensor]['timestamp'] = timestamp
                pilot_model[pilot][sensor]['index'] = 0

            return True

    return False

def get_value(data:dict) -> float:
    return data['index']

def get_data(pilot:str, sensor:str, count:int) -> list:
    results = []

    if pilot in pilot_model and sensor in pilot_model[pilot]:

        for i in range(0,count):
            results.append( [pilot_model[pilot][sensor]['timestamp'], get_value(pilot_model[pilot][sensor]) ])

            t = fiware_to_datetime(pilot_model[pilot][sensor]['timestamp'])
            t += datetime.timedelta(minutes=pilot_model[pilot][sensor]['timestep'])
            pilot_model[pilot][sensor]['timestamp'] = datetime_to_fiware(t)

            pilot_model[pilot][sensor]['index'] += 1

    return results

if __name__ == '__main__':
    quitApp = False

    add_pilot('YYZ')
    add_sensor_to_pilot('YYZ', 'sensor1')
    add_sensor_to_pilot('YYZ', 'sensor2')

    pilot_model['YYZ']['sensor1']['timestep'] = 15  # minutes
    pilot_model['YYZ']['sensor2']['timestep'] = 60  # minutes

    reset_pilot('YYZ', datetime_to_fiware(datetime.datetime.now()))

    while quitApp is False:
        print('\nSDG management')
        print()
        print('1..Reset Sim')


        print('X..Quit')
        print('\n')

        key = input('>')

        if key == 'x':
            quitApp = True

        if key == '1':
           reset_pilot('YYZ', datetime.datetime.now())

        if key =='2':
            data = get_data('YYZ', 'sensor1',1)
            print(json.dumps(data, indent=3))

            data = get_data('YYZ', 'sensor2', 1)
            print(json.dumps(data, indent=3))

        if key == '3':
            data = get_data('YYZ', 'sensor1', 5)
            print(json.dumps(data, indent=3))

            data = get_data('YYZ', 'sensor2', 5)
            print(json.dumps(data, indent=3))