from django.urls import path, include

from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view, permission_classes

from django.http import JsonResponse
from django.http import Http404
from django.http import HttpResponse

import website.sdg
@csrf_exempt
#@permission_classes((IsAuthenticated,))
@api_view(['GET'])
def get_data(request):
    try:
        return JsonResponse(website.sdg.get_data(request.query_params['pilot'], request.query_params['sensor'], int(request.query_params['steps'])), safe=False)
    except Exception as e:
        return HttpResponse(status=500)

    return HttpResponse(status=400)

@csrf_exempt
#@permission_classes((IsAuthenticated,))
@api_view(['GET'])
def get_info(request):
    try:
        return JsonResponse(website.sdg.pilot_model, safe=False)
    except Exception as e:
        return HttpResponse(status=500)

    return HttpResponse(status=400)

@csrf_exempt
#@permission_classes((IsAuthenticated,))
@api_view(['POST'])
def add_pilot(request):
    try:
        if website.sdg.add_pilot(request.query_params['pilot']) == True:
            return HttpResponse(status=201)

    except Exception as e:
        return HttpResponse(status=500)

    return HttpResponse(status=400)

@csrf_exempt
#@permission_classes((IsAuthenticated,))
@api_view(['POST'])
def add_sensor_to_pilot(request):
    try:
        if website.sdg.add_sensor_to_pilot(request.query_params['pilot'], request.query_params['sensor']) == True:
            return HttpResponse(status=201)

    except Exception as e:
        return HttpResponse(status=500)

    return HttpResponse(status=400)

@csrf_exempt
#@permission_classes((IsAuthenticated,))
@api_view(['POST'])
def reset_pilot(request):
    try:
        if website.sdg.reset_pilot(request.query_params['pilot'], request.query_params['time']) == True:
            return HttpResponse(status=200)
    except Exception as e:
        return HttpResponse(status=500)

    return HttpResponse(status=400)

@api_view(['DELETE'])
def delete_pilot(request):
    try:
        if website.sdg.delete_pilot(request.query_params['pilot']) == True:
            return HttpResponse(status=200)
    except Exception as e:
        return HttpResponse(status=500)


    return HttpResponse(status=400)

@api_view(['DELETE'])
def delete_sensor_from_pilot(request):
    try:
        if website.sdg.delete_sensor_from_pilot(request.query_params['pilot'], request.query_params['sensor']) == True:
            return HttpResponse(status=200)
    except Exception as e:
        return HttpResponse(status=500)


    return HttpResponse(status=400)

urlpatterns = [
    path('get_data', get_data),
    path('add_pilot', add_pilot),
    path('add_sensor_to_pilot', add_sensor_to_pilot),
    path('delete_sensor', delete_sensor_from_pilot),
    path('reset_pilot',  reset_pilot),
    path('delete_pilot', delete_pilot),
    path('get_info', get_info),
]
